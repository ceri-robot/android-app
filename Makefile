all:
	@echo nop

upgrade-proto:
	git submodule update --remote 

update-proto:
	git submodule update 

proto: update-proto
	cd proto-interfaces && protoc --java_out=../MCPRemote/mobile/src/main/java/ *.proto

.PHONY: upgrade-proto update-proto proto
