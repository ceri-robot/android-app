package encom.mcpremote.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jmedeisis.bugstick.Joystick;
import com.jmedeisis.bugstick.JoystickListener;

import java.io.IOException;

import encom.mcpremote.R;
import encom.mcpremote.services.RobotComService;
import encom.proto.MovementControl;

public class JoystickControllerFragment extends Fragment implements JoystickListener {

    private int previousSpeedR = 0;
    private int previousSpeedL = 0;
    private MovementControl.MovementControlRequest.MotorControlRequest.Direction previousDirection = null;
    private RobotComService.RobotComInterface robotComInterface = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.joystick_view, container, false);

        Joystick joystick = (Joystick) v.findViewById(R.id.joystick);
        joystick.setJoystickListener(this);

        return v;
    }

    public void setRobotComInterface(RobotComService.RobotComInterface robotComInterface) {
        this.robotComInterface = robotComInterface;
    }


    @Override
    public void onDown() {

    }

    @Override
    public void onDrag(float degrees, float offset) {
        float x = (float) (Math.cos(degrees * Math.PI / 180f) * offset);
        x = Math.abs(x) * -1 + 1;
        int y = (int) (Math.sin(degrees * Math.PI / 180f) * offset * 100);

        if (y == 0) {
            stopWheels();
            return;
        }

        MovementControl.MovementControlRequest.MotorControlRequest.Direction direction = (y >= 0) ? MovementControl.MovementControlRequest.MotorControlRequest.Direction.FORWARD : MovementControl.MovementControlRequest.MotorControlRequest.Direction.BACKWARD;
        int speedR = Math.abs(y);
        int speedL = Math.abs(y);
        if (degrees > -90 && degrees < 90) {
            speedR *= x;
        }

        if (degrees < -90 || degrees > 90) {
            speedL *= x;
        }

        buildAndSendMessage(speedR, speedL, direction);
    }

    @Override
    public void onUp() {
        stopWheels();
    }

    public void stopWheels() {
        buildAndSendMessage(0, 0, MovementControl.MovementControlRequest.MotorControlRequest.Direction.FORWARD);
    }


    public void buildAndSendMessage(int speedR, int speedL, MovementControl.MovementControlRequest.MotorControlRequest.Direction direction) {
        if (previousSpeedR == speedR && previousSpeedL == speedL && previousDirection == direction) {
            return;
        }

        previousSpeedR = speedR;
        previousSpeedL = speedL;
        previousDirection = direction;

        MovementControl.MovementControlRequest basicControlMessage = MovementControl.MovementControlRequest.newBuilder()
                .addMotors(MovementControl.MovementControlRequest.MotorControlRequest.newBuilder()
                        .setDirection(direction)
                        .setMotor(MovementControl.MovementControlRequest.MotorControlRequest.MotorPosition.FRONT_RIGHT)
                        .setSpeed(Math.abs(speedR)))
                .addMotors(MovementControl.MovementControlRequest.MotorControlRequest.newBuilder()
                        .setDirection(direction)
                        .setMotor(MovementControl.MovementControlRequest.MotorControlRequest.MotorPosition.FRONT_LEFT)
                        .setSpeed(Math.abs(speedL)))
                .addMotors(MovementControl.MovementControlRequest.MotorControlRequest.newBuilder()
                        .setDirection(direction)
                        .setMotor(MovementControl.MovementControlRequest.MotorControlRequest.MotorPosition.BACK_RIGHT)
                        .setSpeed(Math.abs(speedR)))
                .addMotors(MovementControl.MovementControlRequest.MotorControlRequest.newBuilder()
                        .setDirection(direction)
                        .setMotor(MovementControl.MovementControlRequest.MotorControlRequest.MotorPosition.BACK_LEFT)
                        .setSpeed(Math.abs(speedL)))
                .build();

        try {
            if (this.robotComInterface != null) {
                robotComInterface.sendCommand(basicControlMessage);
            }
        } catch (IOException e) {
            Log.e("DebugAct", "Unable to sendCommand message", e);
        }
    }
}
