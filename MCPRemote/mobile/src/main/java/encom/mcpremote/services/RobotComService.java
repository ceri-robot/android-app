package encom.mcpremote.services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseArray;

import com.google.protobuf.Any;
import com.google.protobuf.Message;

import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import encom.proto.MCP;

public class RobotComService extends Service {
    private static final ByteOrder NETWORK_BYTE_ORDER = ByteOrder.BIG_ENDIAN;
    private static final String DEFAULT_HOST = "192.168.100.1";
    private static final int DEFAULT_PORT = 1357;

    private final RobotComBinder robotComBinder = new RobotComBinder();
    private RobotComInterface robotComInterface;


    public RobotComService() {
        robotComInterface = null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("RobotComSvc", "onCreate");
        robotComInterface = new RobotComInterface();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("RobotComSvc", "onDestroy");
        if (robotComInterface != null && robotComInterface.isConnected()) {
            try {
                robotComInterface.disconnect();
            } catch (IOException e) {
                Log.e("RobotComSvc", "Unable to disconnect", e);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return robotComBinder;
    }

    public class RobotComInterface {
        private final SparseArray<MCPResponseListener> listenerMap;
        private Socket robotSocket;
        private Thread robotComSocketReceiver;
        private Random random;

        protected RobotComInterface() {
            robotSocket = null;
            robotComSocketReceiver = null;
            listenerMap = new SparseArray<>();
            random = new Random();
        }

        private void startReceiverThread() {
            if (robotComSocketReceiver != null && robotComSocketReceiver.isAlive()) {
                robotComSocketReceiver.interrupt();
            }

            robotComSocketReceiver = new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.d("RobotComI", "SocketReceiverThread started");
                    int zeroReadGuard = 0;
                    try {
                        while (isConnected() && zeroReadGuard < 10 && !robotComSocketReceiver.isInterrupted()) {
                            byte[] buff = new byte[4];
                            int rsize = robotSocket.getInputStream().read(buff);
                            Log.d("RobotComI", "rsize is " + rsize);
                            ByteBuffer bbuff = ByteBuffer.wrap(buff);
                            bbuff.order(NETWORK_BYTE_ORDER);
                            int msgSize = bbuff.getInt();
                            if (msgSize == 0) {
                                zeroReadGuard++;
                                continue;
                            }

                            if (msgSize < 0) {
                                Log.d("RobotComI", "msgSize < 0 (" + msgSize + ")");
                                continue;
                            }

                            zeroReadGuard = 0;
                            Log.d("RobotComI", "Reading message of size " + msgSize);
                            buff = new byte[msgSize];
                            rsize = robotSocket.getInputStream().read(buff);
                            while (rsize != msgSize) {
                                rsize += robotSocket.getInputStream().read(buff, rsize, msgSize - rsize);
                            }

                            MCP.Response response = MCP.Response.parseFrom(buff);

                            // If RequestId is 0, no need to forward response
                            if (response.getRequestId() == 0) {
                                Log.d("RobotComI", "RequestId is 0");
                                continue;
                            }

                            synchronized (listenerMap) {
                                MCPResponseListener awaitingListener = listenerMap.get(response.getRequestId());
                                if (awaitingListener != null) {
                                    if (!awaitingListener.onResponse(response)) {
                                        listenerMap.remove(response.getRequestId());
                                    }
                                } else {
                                    Log.i("RobotComI", "No listener fo requestId " + response.getRequestId());
                                }
                            }
                        }

                        Log.i("RobotComI", "Read thread exited wih zeroReadGuard=" + zeroReadGuard);
                    } catch (IOException e) {
                        Log.e("RobotComI", "Error on Reading Thread", e);
                    }

                    try {
                        disconnect();
                    } catch (IOException e) {
                        Log.e("RobotComI", "Error while disconnecting", e);
                    }
                }
            });
            robotComSocketReceiver.setName("robotComSocketReceiver");
            robotComSocketReceiver.setDaemon(true);
            robotComSocketReceiver.start();
        }

        private void connect() throws IOException {
            if (isConnected()) {
                return;
            }

            try {
                final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(RobotComService.this.getBaseContext());
                // creating socket in another thread
                Log.d("RobotComI", "Creating socket");
                robotSocket = Executors.newSingleThreadExecutor().submit(new Callable<Socket>() {
                    @Override
                    public Socket call() throws Exception {
                        Socket s;
                        int customServerPort = DEFAULT_PORT;
                        String customServerHost = preferences.getString("pref_key_custom_server", DEFAULT_HOST);
                        if (preferences.getBoolean("pref_key_use_custom_server", false) && null != customServerHost) {
                            if (customServerHost.contains(":")) {
                                customServerPort = Integer.parseInt(customServerHost.split(":", 2)[1]);
                            }

                            Log.d("RobotComI", "Connecting to custom server: " + customServerHost + ":" + customServerPort);
                            s = new Socket(customServerHost, customServerPort);
                        } else {
                            Log.d("RobotComI", "Connecting to default server");
                            s = new Socket(DEFAULT_HOST, DEFAULT_PORT);
                        }
                        s.setKeepAlive(true);

                        return s;
                    }
                }).get();
                startReceiverThread();
            } catch (InterruptedException | ExecutionException e) {
                Log.e("RobotComI", "Error while establishing socket", e.getCause());

                throw (IOException) e.getCause();
            }
        }

        private void disconnect() throws IOException {
            if (isConnected()) {
                Log.d("RobotComI", "Disconnecting socket");
                robotSocket.close();
                Log.d("RobotComI", "Socket is closed");

                if (robotComSocketReceiver != null && robotComSocketReceiver.isAlive()) {
                    robotComSocketReceiver.interrupt();
                }
            }
        }

        private boolean isConnected() {
            return robotSocket != null && robotSocket.isConnected() && !robotSocket.isClosed();
        }

        public void sendCommand(Message requestMessage) throws IOException {
            MCP.Request request = MCP.Request.newBuilder()
                    .setMessage(Any.pack(requestMessage))
                    .setRequestId(0)
                    .build();

            writeToSocket(request);
        }

        public void sendCommandWithResponse(Message requestMessage, MCPResponseListener responseListener) throws IOException {
            int generatedRequestId;
            synchronized (listenerMap) {
                do {
                    generatedRequestId = random.nextInt(Integer.MAX_VALUE - 1) + 1;
                } while (listenerMap.get(generatedRequestId) != null);
                listenerMap.put(generatedRequestId, responseListener);
            }


            Log.d("RobotComI", "sendCommandWithResponse requestId=" + generatedRequestId);

            MCP.Request request = MCP.Request.newBuilder()
                    .setMessage(Any.pack(requestMessage))
                    .setRequestId(generatedRequestId)
                    .build();

            writeToSocket(request);
        }

        private void writeToSocket(MCP.Request request) throws IOException {
            if (!isConnected()) {
                connect();
            }

            byte[] messageBytes = request.toByteArray();
            //Log.d("RobotComI", "Writing message of size " + messageBytes.length);

            ByteBuffer b = ByteBuffer.allocate(messageBytes.length + 4);
            b.order(NETWORK_BYTE_ORDER);
            b.putInt(messageBytes.length);
            b.put(messageBytes);

            robotSocket.getOutputStream().write(b.array());
        }
    }

    public class RobotComBinder extends Binder {
        public RobotComInterface getComInterface() {
            return robotComInterface;
        }
    }
}
