package encom.mcpremote.services;

import encom.proto.MCP;

public interface MCPResponseListener {
    boolean onResponse(MCP.Response response);
}
