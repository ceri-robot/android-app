package encom.mcpremote;

import java.util.List;
import java.util.Random;

import encom.proto.ImageRecognition;

public class ImageRecognitionMessageGenerator {
    private static final Random RANDOM = new Random();
    private static String[] introGeneric = new String[]{
            "I found ",
            "I see ",
            "It's ",
            "I guess it's "
    };
    private static String[] introHighScore = new String[]{
            "I'm pretty sure it's ",
            "I'm almost certain it's ",
            "It's certainly "
    };

    public static String generateMessageFor(List<ImageRecognition.ImageRecognitionResponse.RecognitionEntry> recognitionEntryList) {
        StringBuilder sb = new StringBuilder();

        int len = recognitionEntryList.size();
        for (int i = 0; i < len; i++) {
            ImageRecognition.ImageRecognitionResponse.RecognitionEntry e = recognitionEntryList.get(i);
            String humanMessage = splitMessage(e.getHuman());

            if (i == 0) {
                if (e.getScore() >= 1) {
                    return "Oh. You're definitely not clever enough for me to describe this picture.";
                }

                if (e.getScore() >= 0.98) {
                    return "It's a bingo ! You showed me " + humanMessage;
                }

                if (e.getScore() >= 0.90) {
                    return "Oh, I'm sure it's " + humanMessage + " !";
                }

                if (e.getScore() >= 0.75) {
                    return introHighScore[RANDOM.nextInt(introHighScore.length)] + humanMessage + ".";
                }

                if (e.getScore() <= 0.30) {
                    sb.append("I'm not sure, but I think it's ");
                } else if (e.getScore() <= 0.20) {
                    sb.append("I can hardly see, i'm unsure but it might be ");
                } else {
                    sb.append(introGeneric[RANDOM.nextInt(introGeneric.length)]);
                }
            }

            sb.append(humanMessage);

            if (e.getScore() < .05f) {
                break;
            }

            if (i != len - 1) {
                sb.append("; or ");
            }
        }

        return sb.append(".").toString();
    }

    private static String splitMessage(String message) {
        return message.replaceAll(", ", "; or ");
    }
}
