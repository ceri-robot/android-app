package encom.mcpremote.activities;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.protobuf.InvalidProtocolBufferException;

import java.io.IOException;
import java.util.Locale;

import encom.mcpremote.ImageRecognitionMessageGenerator;
import encom.mcpremote.R;
import encom.mcpremote.fragments.JoystickControllerFragment;
import encom.mcpremote.services.MCPResponseListener;
import encom.mcpremote.services.RobotComService;
import encom.proto.ImageRecognition;
import encom.proto.MCP;

import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;


public class ControllerActivity extends AppCompatActivity {
    private static final String DEFAULT_HOST = "192.168.100.1";
    private static final int DEFAULT_PORT = 1358;
    private ProgressDialog connectionProgressDialog = null;
    private TextToSpeech tts = null;
    private boolean ttsAvailable = false;
    private RobotComService.RobotComInterface robotComInterface = null;
    private JoystickControllerFragment joystickControllerFragment;
    private ImageView cameraView;
    private ProgressBar scanBarView;
    private ImageButton captureCameraImageButton;
    private boolean capturingCamera = false;
    private ColorFilter grayscaleColorFilter = null;
    private WebView streamWebView;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            robotComInterface = ((RobotComService.RobotComBinder) service).getComInterface();
            joystickControllerFragment.setRobotComInterface(robotComInterface);
            String statusText;
            try {
                robotComInterface.sendCommand(encom.proto.Status.StatusRequest.newBuilder().build());
                toggleControlsVisibility(true);
                statusText = "Connected to M C P";
                streamWebView = (WebView) findViewById(R.id.streamWebView);
                final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ControllerActivity.this.getBaseContext());
                int customServerPort = DEFAULT_PORT;
                String customServerHost = DEFAULT_HOST;
                if (preferences.getBoolean("pref_key_use_custom_server", false)) {
                    customServerHost = preferences.getString("pref_key_custom_server", DEFAULT_HOST);
                }

                Log.e("URL", "<img src='http://" + customServerHost + ":" + customServerPort + "' />");
                streamWebView.loadData("<html><head></head><body style='margin:0'><img style='height:100%; margin-left:80px' src='http://" + customServerHost + ":" + customServerPort + "/stream.mjpeg' /></html>", "text/html", "utf-8");
            } catch (IOException e) {
                toggleControlsVisibility(false);
                statusText = "M C P not reachable... Try to change connection properties";
            }

            connectionProgressDialog.dismiss();
            speak(statusText);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            robotComInterface = null;
            joystickControllerFragment.setRobotComInterface(null);
        }
    };

    @Override
    protected void onStart() {
        super.onStart();

        if (robotComInterface == null) {
            if (connectionProgressDialog != null) {
                connectionProgressDialog.dismiss();
            }
            connectionProgressDialog = ProgressDialog.show(ControllerActivity.this, "", "Trying to establish connection to the robot... ", true);
            Intent intent = new Intent(this, RobotComService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (robotComInterface != null) {
            unbindService(mConnection);
            robotComInterface = null;
            joystickControllerFragment.setRobotComInterface(null);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(SCREEN_ORIENTATION_LANDSCAPE);

        // set default settings
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        setContentView(R.layout.activity_controller);
        joystickControllerFragment = (JoystickControllerFragment) getFragmentManager().findFragmentById(R.id.joystick);
        cameraView = (ImageView) findViewById(R.id.cameraView);
        captureCameraImageButton = (ImageButton) findViewById(R.id.captureCameraImage);
        scanBarView = (ProgressBar) findViewById(R.id.scanBar);

        captureCameraImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureImage();
            }
        });

        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                ttsAvailable = status == TextToSpeech.SUCCESS;
                if (status == TextToSpeech.SUCCESS) {
                    tts.setLanguage(Locale.ENGLISH);
                    tts.setSpeechRate(0.95f);
                } else {
                    tts = null;
                }
            }
        });

        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        grayscaleColorFilter = new ColorMatrixColorFilter(cm);
    }

    public void openPreferences(View view) {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    private void captureImage() {
        if (capturingCamera) {
            speak("There is already an image recognition running.");

            return;
        }

        speak("Capturing image..");
        capturingCamera = true;
        captureCameraImageButton.setImageAlpha(50);
        try {
            robotComInterface.sendCommandWithResponse(ImageRecognition.ImageRecognitionRequest.newBuilder().build(), new MCPResponseListener() {

                @Override
                public boolean onResponse(MCP.Response response) {
                    try {
                        ImageRecognition.ImageRecognitionResponse irResponse = response.getMessage().unpack(ImageRecognition.ImageRecognitionResponse.class);
                        final Bitmap bitmap = BitmapFactory.decodeByteArray(irResponse.getImage().toByteArray(), 0, irResponse.getImage().size());
                        Log.d("captureImage", "Image size is " + bitmap.getWidth() + "x" + bitmap.getHeight());
                        captureCameraImageButton.post(new Runnable() {
                            @Override
                            public void run() {
                                cameraView.setVisibility(View.VISIBLE);
                                scanBarView.setVisibility(View.VISIBLE);
                                cameraView.setColorFilter(grayscaleColorFilter);
                                cameraView.setImageBitmap(bitmap);
                            }
                        });

                        if (!irResponse.getRecognitionComplete()) {
                            speak("Starting image recognition ...");
                        } else {
                            Log.d("ImageReco", "Reco completed results follow");
                            for (ImageRecognition.ImageRecognitionResponse.RecognitionEntry e : irResponse.getRecognitionEntriesList()) {
                                Log.d("ImageReco", e.getScore() + " : " + e.getHuman());
                            }

                            speak("Image recognition complete ...");
                            speak(ImageRecognitionMessageGenerator.generateMessageFor(irResponse.getRecognitionEntriesList()));

                            capturingCamera = false;
                            captureCameraImageButton.post(new Runnable() {
                                @Override
                                public void run() {
                                    captureCameraImageButton.setImageAlpha(255);
                                    cameraView.setVisibility(View.VISIBLE);
                                    scanBarView.setVisibility(View.GONE);
                                    cameraView.setColorFilter(null);
                                }
                            });

                            return false;
                        }

                        return true;
                    } catch (InvalidProtocolBufferException e) {
                        e.printStackTrace();
                    }

                    return true;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void speak(String text) {
        if (ttsAvailable & tts != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tts.speak(text, TextToSpeech.QUEUE_ADD, null, null);
            } else {
                tts.speak(text, TextToSpeech.QUEUE_ADD, null);
            }
        } else {
            Toast toast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void toggleControlsVisibility(boolean visible) {
        int visibility = visible ? View.VISIBLE : View.INVISIBLE;
        cameraView.setVisibility(visibility);
        captureCameraImageButton.setVisibility(visibility);
        if (joystickControllerFragment.getView() != null) {
            joystickControllerFragment.getView().setVisibility(visibility);
        }

        if (!visible) {
            scanBarView.setVisibility(visibility);
        }
    }

    public void hideCameraView(View view) {
        view.setVisibility(View.GONE);
    }
}
